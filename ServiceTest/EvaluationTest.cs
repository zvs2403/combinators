﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.WebSockets;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using KPSint;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KPSint.List;

namespace ServiceTest
{
    [TestClass]
    public class EvaluationTest
    {
        [TestMethod]
        public void OptimizeTest()
        {
            string[] beginstr = { "(BWB)a((BWB)a)", "WS(B)a", "(WSB)bya(arf(yg)j)j", "WS(ghT((dfds)(thGB)fhf)h)II", "WS(ghT(dfds(thGB)fhf)h)II", "WS((ffH)fhg((gfT))y)gh(y)", "(BWB)a((BWB)a)" };
            string[] ideal = { "BWBa(BWBa)", "WSBa", "WSBbya(arf(yg)j)j", "WS(ghT(dfds(thGB)fhf)h)II", "WS(ghT(dfds(thGB)fhf)h)II", "WS(ffHfhg(gfT)y)ghy", "BWBa(BWBa)" };
            string info = "";

            for (int i = 0; i < ideal.Length; i++)
            {
                StringBuilder testStr = new StringBuilder(beginstr[i]);
                NodeList testList = Parser.StringToList(testStr);
                try
                {
                    Evaluation.Optimize(testList);
                }
                catch (NullReferenceException e)
                {
                    info = e.StackTrace;
                    testList = null;
                }
                StringBuilder testRes = Parser.ListToString(testList);
                string resstr = testRes.ToString();
                Assert.AreEqual(ideal[i], resstr, info);
            }        
        }

        [TestMethod]
        public void EvalTest()
        {
            string[] beginstr = {"WS(BWB)a", "S(BWB)(BWB)a"};
            string[] idealsrt = { "S(BWB)(BWB)a", "BWBa(BWBa)" };

            for (int i = 0; i < idealsrt.Length; i++)
            {
                StringBuilder testStr = new StringBuilder(beginstr[i]);

                NodeList testList = Parser.StringToList(testStr);

                testList = Evaluation.Eval(testList);

                StringBuilder resStr = Parser.ListToString(testList);

                Assert.AreEqual(idealsrt[i], resStr.ToString(), "fail");
            }
            
        }

        [TestMethod]
        public void UpListTest()
        {
            string [] beginstr = {"WS(BWB)a", "WS(B)a", "WS(ghT(dfds(thGB)fhf)h)II", "WS(ghT(dfds(thGB)fhf)h)II" , "WS(ghT(dfds(thGB)fhf)h)II", "(BWB)a((BWB)a)", "BWBa((BWB)a)" };
            string[] ideal = {"WSBWBa", "WSBa", "WSghT(dfds(thGB)fhf)hII", "WS(ghTdfds(thGB)fhfh)II", "WS(ghT(dfdsthGBfhf)h)II", "BWBa((BWB)a)", "BWBa(BWBa)" };
            StringBuilder resStr;

            for (int i = 0; i < ideal.Length; i++)
            {
                StringBuilder testStr = new StringBuilder(beginstr[i]);

                NodeList testList = Parser.StringToList(testStr);
                try
                {
                    switch (i)
                    {
                        case 0:
                        {
                            Evaluation.UpList(testList[2].List);
                            resStr = Parser.ListToString(testList);
                            Assert.AreEqual(ideal[i], resStr.ToString(), "fail in #1");
                            break;
                        }
                        case 1:
                        {
                            Evaluation.UpList(testList[2].List);
                            resStr = Parser.ListToString(testList);
                            Assert.AreEqual(ideal[i], resStr.ToString(), "fail in #2");
                            break;
                        }
                        case 2:
                        {
                            Evaluation.UpList(testList[2].List);
                            resStr = Parser.ListToString(testList);
                            Assert.AreEqual(ideal[i], resStr.ToString(), "fail in #3");
                            break;
                        }
                        case 3:
                        {
                            Evaluation.UpList(testList[2].List[3].List);
                            resStr = Parser.ListToString(testList);
                            Assert.AreEqual(ideal[i], resStr.ToString(), "fail in #4");
                            break;
                        }
                        case 4:
                        {
                            Evaluation.UpList(testList[2].List[3].List[4].List);
                            resStr = Parser.ListToString(testList);
                            Assert.AreEqual(ideal[i], resStr.ToString(), "fail in #5");
                            break;
                        }

                        case 5:
                        {
                            Evaluation.UpList(testList[0].List);
                            resStr = Parser.ListToString(testList);
                            Assert.AreEqual(ideal[i], resStr.ToString(), "fail in #6");
                            break;
                        }

                        case 6:
                        {
                            Evaluation.UpList(testList[2].List[0].List);
                            resStr = Parser.ListToString(testList);
                            Assert.AreEqual(ideal[i], resStr.ToString(), "fail in #7");
                            break;
                        }
                    }
                }
                catch (NullReferenceException e)
                {
                    string info = e.StackTrace + " in number " + i;
                }

            }

        }

        [TestMethod]
        public void LookingForCombTest()
        {
            string[] beginstr = {"WS(BWB)a", "addSW(IKdfC)S", "dg(IhKKKS)gh", "dd(gf(HV(fgg)rtH((hg))))hj((fgC(qw)zT)o)"};
            int[] idealIndex = {0, 3, 0, 2};
            char[] idealMeaning = {'W', 'S', 'I', 'C'};
            int i = 0;
            try
            {
                for (i = 0; i < idealMeaning.Length; i++)
                {
                    StringBuilder testStr = new StringBuilder(beginstr[i]);
                    NodeList testList = Parser.StringToList(testStr);
                    (NodeList changeList, int index) tuple = Evaluation.LookingForComb(testList);
                    Assert.AreEqual(idealIndex[i], tuple.index);
                    Assert.IsNotNull(tuple.changeList[tuple.index].elem);
                    Assert.AreEqual(idealMeaning[i], tuple.changeList[tuple.index].elem.meaning);
                }
            }
            catch (NullReferenceException e)
            {
                string info = e.Message + " " + i;
                Assert.AreEqual(0, 1, info);
            }

        }

        [TestMethod]
        public void DetailEvalTest()
        {
            string[] beginstr = { "WS(BWB)a", "S(BWB)(BWB)a" };
            string[] idealsrt = { "S(BWB)(BWB)a", "BWBa(BWBa)" };

            for (int i = 0; i < idealsrt.Length; i++)
            {
                StringBuilder testStr = new StringBuilder(beginstr[i]);

                NodeList testList = Parser.StringToList(testStr);

                testList = Evaluation.Eval(testList);

                //Assert.AreEqual(4, testList.Count);

                StringBuilder resStr = Parser.ListToString(testList);

                Assert.AreEqual(idealsrt[i], resStr.ToString(), "fail");
            }



        }

        [TestMethod]
        public void OptimizeOneTest()
        {
            string[] beginstr = { "WS((ffH)fhg((gfT))y)gh(y)", "(BWB)a((BWB)a)" };
            string[] ideal = { "WS(ffHfhg(gfT)y)ghy", "BWBa(BWBa)" };
            string info = "";

            for (int i = 0; i < ideal.Length; i++)
            {
                StringBuilder testStr = new StringBuilder(beginstr[i]);
                NodeList testList = Parser.StringToList(testStr);
                try
                {
                    Evaluation.Optimize(testList);
                    if (i == 5)
                    {
                        throw new NullReferenceException("problem with " + i);
                    }
                }
                catch (NullReferenceException e)
                {
                    info = e.StackTrace;
                    testList = null;
                }
                catch (ArgumentOutOfRangeException e)
                {
                    info = e.StackTrace;
                    testList = null;
                }
                StringBuilder testRes = Parser.ListToString(testList);
                string resstr = testRes.ToString();
                Assert.AreEqual(ideal[i], resstr, info);

            }
        }
        [TestMethod]
        public void UpListTest1()
        {
            string[] beginstr = { "WS((ffH)fhg((gfT))y)gh(y)" };
            string[] ideal = { "WS(ffHfhg(gfT)y)ghy" };
            string info = "";


            StringBuilder testStr = new StringBuilder(beginstr[0]);
            NodeList testList = Parser.StringToList(testStr);
            NodeList sublist1 = testList[2].List[4].List;
            try
            {
                Evaluation.UpList(testList[2].List[4].List);
                Evaluation.UpList(testList[2].List[4].List);

            }
            catch (NullReferenceException e)
            {
                info = e.StackTrace;
                testList = null;
            }
            StringBuilder testRes = Parser.ListToString(testList);
            string resstr = testRes.ToString();
            Assert.AreEqual(ideal[0], resstr, info);
        }
    }
}
