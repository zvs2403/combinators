﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KPSint;
using KPSint.Combinators;
using KPSint.List;

namespace ServiceTest
{
    [TestClass]
    public class CombTest
    {
        [TestMethod]
        public void WTest()
        {
            W testComb = new W();

            Const testop1 = new Const('x');
            Const testop2 = new Const('y');
            Const testop3 = new Const('z');
            
            Node op1 = new Node(testop1, null);
            Node op2 = new Node(testop2, null);
            Node op3 = new Node(testop3, null);

            NodeList testList = new NodeList() { op1, op2, op3 };

            Assert.AreEqual(2, testComb.Arity);
            Assert.AreEqual('W', testComb.Meaning);

            List<Node> resList = testComb.Function(testList);

            Assert.AreEqual(3, resList.Count);
            Assert.AreEqual(testList[0], resList[0], "problem in first elem");
            Assert.AreEqual(testList[1], resList[1], "problem in second elem");
            Assert.AreEqual(testList[1], resList[2], "problem in second elem");
            Assert.AreEqual('x', resList[0].elem.meaning);
            Assert.AreEqual('y', resList[1].elem.meaning);
            Assert.AreEqual('y', resList[2].elem.meaning);
            Assert.IsNull(resList[0].list);
            Assert.IsNull(resList[1].list);
            Assert.IsNull(resList[2].list);

        }

        [TestMethod]
        public void CTest()
        {
            C testComb = new C();

            Const testop1 = new Const('x');
            Const testop2 = new Const('y');
            Const testop3 = new Const('z');

            Node op1 = new Node(testop1, null);
            Node op2 = new Node(testop2, null);
            Node op3 = new Node(testop3, null);

            NodeList testList = new NodeList() { op1, op2, op3 };

            Assert.AreEqual(3, testComb.Arity);
            Assert.AreEqual('C', testComb.Meaning);

            List<Node> resList = testComb.Function(testList);

            Assert.AreEqual(3, resList.Count);
            Assert.AreEqual(testList[0], resList[0]);
            Assert.AreEqual(testList[1], resList[2]);
            Assert.AreEqual(testList[2], resList[1]);
            Assert.AreEqual('x', resList[0].elem.meaning);
            Assert.AreEqual('z', resList[1].elem.meaning);
            Assert.AreEqual('y', resList[2].elem.meaning);
            Assert.IsNull(resList[0].list);
            Assert.IsNull(resList[1].list);
            Assert.IsNull(resList[2].list);

        }

        [TestMethod]
        public void BTest()
        {
            B testComb = new B();

            Const testop1 = new Const('x');
            Const testop2 = new Const('y');
            Const testop3 = new Const('z');

            Node op1 = new Node(testop1, null);
            Node op2 = new Node(testop2, null);
            Node op3 = new Node(testop3, null);

            NodeList testList = new NodeList(){ op1, op2, op3 };

            Assert.AreEqual(3, testComb.Arity);
            Assert.AreEqual('B', testComb.Meaning);

            NodeList resList = testComb.Function(testList);

            try
            {
                Assert.AreEqual(2, resList.Count);
                Assert.AreSame(testList[0], resList[0]);
                Assert.IsNotNull(resList[1].List, "some shit with second elem - List");
                Assert.IsNull(resList[1].elem);
                Assert.AreEqual(2, resList[1].List.Count);
                Assert.AreSame(testList[1], resList[1].List[0]);
                Assert.AreSame(testList[2], resList[1].List[1]);
                Assert.AreEqual('x', resList[0].elem.meaning);
                Assert.AreEqual('y', resList[1].List[0].elem.meaning);
                Assert.AreEqual('z', resList[1].List[1].elem.meaning);
                Assert.IsNull(resList[0].List);
                Assert.IsNull(resList[1].List[0].List);
                Assert.IsNull(resList[1].List[1].List);
                Assert.AreSame(resList, resList[1].List.prevList);
                Assert.AreEqual(1, resList[1].List.prevIndex);
            }
            catch (NullReferenceException e)
            {
                Assert.AreEqual(1, 2, e.StackTrace);
            }
        }

        [TestMethod]
        public void STest()
        {
            S testComb = new S();

            Const testop1 = new Const('x');
            Const testop2 = new Const('y');
            Const testop3 = new Const('z');

            Node op1 = new Node(testop1, null);
            Node op2 = new Node(testop2, null);
            Node op3 = new Node(testop3, null);

            NodeList testList = new NodeList() { op1, op2, op3 };

            Assert.AreEqual(3, testComb.Arity);
            Assert.AreEqual('S', testComb.Meaning);

            NodeList resList = testComb.Function(testList);

            try
            {
                Assert.AreEqual(3, resList.Count);
                Assert.AreSame(testList[0], resList[0]);
                Assert.AreSame(testList[2], resList[1]);
                Assert.IsNotNull(resList[2].List, "some shit with second elem - List");
                Assert.IsNull(resList[2].elem);
                Assert.AreEqual(2, resList[2].List.Count);
                Assert.AreSame(testList[1], resList[2].List[0]);
                Assert.AreSame(testList[2], resList[2].List[1]);
                Assert.AreEqual('x', resList[0].elem.meaning);
                Assert.AreEqual('z', resList[1].elem.meaning);
                Assert.AreEqual('y', resList[2].List[0].elem.meaning);
                Assert.AreEqual('z', resList[2].List[1].elem.meaning);
                Assert.IsNull(resList[0].List);
                Assert.IsNull(resList[1].List);
                Assert.IsNull(resList[2].List[0].List);
                Assert.IsNull(resList[2].List[1].List);
                Assert.AreSame(resList, resList[2].List.prevList);
                Assert.AreEqual(2, resList[2].List.prevIndex);
            }
            catch (NullReferenceException e)
            {
                Assert.AreEqual(1, 2, e.StackTrace);
            }
        }
    }
}
