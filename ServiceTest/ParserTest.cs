﻿using System;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using KPSint;
using KPSint.List;
using KPSint.Combinators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ServiceTest
{
    [TestClass]
    public class ParserTest
    {
        [TestMethod]
        public void ListToStringTest()
        {
            StringBuilder inputStr = new StringBuilder("WS((ffH)fhg((gfT))y)gh(y)");

            NodeList resList = Parser.StringToList(inputStr);

            ShowList(resList);

            Assert.IsNotNull(resList);
            NodeList sublist1 = resList[2].List;
            NodeList sublist2 = resList[5].List;
            ShowList(sublist1);
            ShowList(sublist2);

            Assert.AreEqual(6, resList.Count);
            Assert.AreEqual('W', resList[0].Meaning);
            Assert.AreEqual('S', resList[1].Meaning);
            Assert.AreEqual('g', resList[3].Meaning);
            Assert.AreEqual('h', resList[4].Meaning);
            Assert.IsFalse(resList[0] is Listl);
            Assert.IsFalse(resList[1] is Listl);
            Assert.IsFalse(resList[3] is Listl);
            Assert.IsFalse(resList[4] is Listl);
            Assert.IsTrue(resList[2] is Listl);
            Assert.IsNotNull(resList[2].List);
            Assert.IsTrue(resList[5] is Listl);
            Assert.IsNotNull(resList[5].List);

            Assert.AreEqual(6, sublist1.Count);
            Assert.IsNull(sublist1[0].elem);
            Assert.IsNotNull(sublist1[0].List);
            Assert.AreEqual('f', sublist1[1].elem.meaning);
            Assert.AreEqual('h', sublist1[2].elem.meaning);
            Assert.AreEqual('g', sublist1[3].elem.meaning);
            Assert.AreEqual('y', sublist1[5].elem.meaning);
            Assert.IsNull(sublist1[4].elem);
            Assert.IsNotNull(sublist1[4].List);
            Assert.AreEqual('y', sublist1[5].elem.meaning);
            Assert.AreSame(resList, sublist1.prevList);
            Assert.AreEqual(2, sublist1.prevIndex);
            Assert.IsNull(sublist1[1].List);
            Assert.IsNull(sublist1[2].List);
            Assert.IsNull(sublist1[3].List);
            Assert.IsNull(sublist1[5].List);

            NodeList sublist3 = sublist1[0].List;
            Assert.AreEqual(3, sublist3.Count);
            Assert.AreEqual('f', sublist3[0].elem.meaning);
            Assert.AreEqual('f', sublist3[1].elem.meaning);
            Assert.AreEqual('H', sublist3[2].elem.meaning);
            Assert.AreSame(sublist1, sublist3.prevList);
            Assert.AreEqual(0, sublist3.prevIndex);
            Assert.IsNull(sublist3[0].List);
            Assert.IsNull(sublist3[1].List);
            Assert.IsNull(sublist3[2].List);


            NodeList sublist4 = sublist1[4].List;
            Assert.AreEqual(1, sublist4.Count);
            Assert.IsNull(sublist4[0].elem);
            Assert.IsNotNull(sublist4[0].List);
            Assert.AreSame(sublist1, sublist4.prevList);
            Assert.AreEqual(4, sublist4.prevIndex);

            NodeList sublist5 = sublist4[0].List;
            Assert.AreEqual(3, sublist5.Count);
            Assert.AreEqual('g', sublist5[0].elem.meaning);
            Assert.AreEqual('f', sublist5[1].elem.meaning);
            Assert.AreEqual('T', sublist5[2].elem.meaning);
            Assert.AreSame(sublist4, sublist5.prevList);
            Assert.AreEqual(0, sublist5.prevIndex);
            Assert.IsNull(sublist5[0].List);
            Assert.IsNull(sublist5[1].List);
            Assert.IsNull(sublist5[2].List);

            Assert.AreEqual(1, sublist2.Count);
            Assert.AreEqual('y', sublist2[0].elem.meaning);
            Assert.IsNull(sublist2[0].List);
            Assert.AreSame(resList, sublist2.prevList);
            Assert.AreEqual(5, sublist2.prevIndex);

            //Evaluation.Optimize(resList);
            /*Assert.AreEqual('B', resList[3].elem.Meaning);
            Assert.AreEqual('d', resList[4].elem.Meaning);

            Assert.AreEqual('I', resList[2].List[0].elem.Meaning);
            Assert.AreEqual('f', resList[2].List[1].elem.Meaning);
            Assert.AreEqual('g', resList[2].List[2].elem.Meaning);
            Assert.AreEqual('B', resList[2].List[3].elem.Meaning);
            Assert.AreEqual(4, resList[2].List.Count);*/
        }

        [TestMethod]
        public void StringToListToString()
        {
            StringBuilder inputStr = new StringBuilder("S((S(KS)K)(S(KS)K)S)(KK)xyz");

            NodeList testList = Parser.StringToList(inputStr);

            StringBuilder resStr = Parser.ListToString(testList);

            Assert.AreEqual(inputStr.ToString(), resStr.ToString());

        }

        public void ShowList(NodeList list)
        {
            Trace.WriteLine("--------------------------------------");
            foreach (var node in list)
            {
                Trace.WriteLine(node.elem);
            }
            Trace.WriteLine("--------------------------------------------------");
        }

        [TestMethod]
        public void ShowMeListTest()
        {
            string[] beginstr = { "WS(BWB)a", "S(BWB)(BWB)a" };
            string[] idealsrt = { "WS(BWB)a", "S(BWB)(BWB)a" };

            for (int i = 0; i < idealsrt.Length; i++)
            {
                StringBuilder testStr = new StringBuilder(beginstr[i]);

                NodeList testList = Parser.StringToList(testStr);

                string testres = Parser.ShowMeAllLists(testList);

                MessageBox.Show(testres);

                //Assert.AreEqual(idealsrt[i], testres, "fail");
            }
        }
    }
}
