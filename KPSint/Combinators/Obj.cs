﻿using KPSint.List;
using Newtonsoft.Json;

namespace KPSint.Combinators
{
    public class Obj
    {

		public Obj(string m)
		{
			Meaning = m;
		}

		public Obj(){ }

        [JsonProperty("arity")]
        public int Arity { get; set; }

        [JsonProperty("result")]
        public NodeList Result { get; set; }

        public virtual NodeList Function(NodeList test)
        {
            return null;
        }
        public string Meaning { get; set; }

		public NodeList List { get; set; }

		public virtual Obj Clone()
		{
			return new Obj(Meaning);
		}

    }
}