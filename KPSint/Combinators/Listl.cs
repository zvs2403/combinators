﻿using KPSint.List;

namespace KPSint.Combinators
{
    public class Listl : Obj
	{

		public Listl(NodeList list)
		{
			List = list;
		}

		public override Obj Clone()
		{
			return new Listl((NodeList) List.Clone());
		}

	}
}
