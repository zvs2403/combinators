﻿

namespace KPSint.Combinators
{
    class Single : Obj
    {
        public Single(string meaning)
        {
            Meaning = meaning;
        }

        public override Obj Clone()
        {
            return new Single(Meaning);
        }
    }
}
