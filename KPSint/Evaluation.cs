﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using KPSint.List;
using KPSint.Combinators;
using KPSint.Service;

namespace KPSint
{
    public class Evaluation
    {
        public static NodeList Eval(NodeList input, Dictionary<string, Combinator> combinators)
        {
            NodeList currentList;
            Debug.WriteLine(input.ToString());
            int index;
            (NodeList changeList, int index) tuple;
            Optimize(input);

            tuple = LookingForComb(input, combinators);
            if (tuple.changeList == null)
            {
                return input;
            }

            currentList = tuple.changeList;
            index = tuple.index;
            Debug.WriteLine("combinator " + tuple.changeList[tuple.index].Meaning);

            StringBuilder s = Parser.ListToString(input);

            Debug.WriteLine("m " + s);
            Debug.WriteLine(index);
            Debug.WriteLine(currentList[index].Arity);
            Debug.WriteLine(tuple.changeList.Count);
            Debug.WriteLine(index + currentList[index].Arity < tuple.changeList.Count);
            if (!(index + combinators[currentList[index].Meaning].Arity < tuple.changeList.Count))
            {
                Debug.WriteLine(index);
                Debug.WriteLine(currentList[index].Arity);
                Debug.WriteLine(currentList.Count);
                return input;
            }

            Execute(tuple, combinators);

            Optimize(input);

            return input;
        }

        public static (NodeList, int) LookingForComb(NodeList inputlist, Dictionary<string, Combinator> combinators)
        {
            Debug.WriteLine(combinators.ContainsKey("S"));
            for (int i = 0; i < inputlist.Count; i++)
            {
                if (!(inputlist[i] is Listl))
                {
                    Debug.WriteLine(inputlist[i].Meaning);
                    if (combinators.ContainsKey(inputlist[i].Meaning))
                    {
                        return (inputlist, i);
                    }
                }
                else
                {
                    NodeList currentList;
                    int index;
                    (currentList, index) = LookingForComb(inputlist[i].List, combinators);
                    if (currentList != null)
                    {
                        return (currentList, index);
                    }
                }
            }
            return (null, -1);
        }

        //public static (NodeList, int) LookingForDeppestComb(NodeList inputlist, NodeList currentList, int currentIndex)
        //{
        //    NodeList ideaList;
        //    int idealIndex;

        //    for (int i = 0; i < inputlist.Count; i++)
        //    {
        //        if (inputlist[i].elem != null)
        //        {
        //            if (!inputlist[i].elem.GetType().ToString().Contains("Const") && inputlist[i].elem.enable)
        //            {
        //                return (inputlist, i);
        //            }
        //        }
        //        else
        //        {
        //            NodeList currentList;
        //            int index;
        //            (currentList, index) = LookingForComb(inputlist[i].List);
        //            if (currentList != null)
        //            {
        //                return (currentList, index);
        //            }
        //        }
        //    }
        //    return (null, -1);
        //} 

        /*public static void EndlessEval(object inputo)
        {
            string result = "";
            Container input = (Container) inputo;
            int i = input.ind;
            NodeList beginlist = input.List;
            TextBlock CalculationResult = input.res;
            Dispatcher disp = input.disp; 

            StringBuilder oldstring = Parser.ListToString(beginlist);
            NodeList newlist = Eval(beginlist);
            StringBuilder newstring = Parser.ListToString(newlist);

            while (!oldstring.Equals(newstring) && i < 100 || _interrupt)
            {
                result += (i+1) + ": ";
                result += newstring + "\n";
                oldstring = newstring;
                newlist = Eval(newlist);
                Optimize(newlist);
                newstring = Parser.ListToString(newlist);
                
                disp.BeginInvoke(DispatcherPriority.Normal,
                    (ThreadStart)delegate ()
                    {
                        CalculationResult.Text += result;
                    }
                );
                Thread.Sleep(50);
                i++;
                result = "";
                
            }

            if (interrupt)
            {
                disp.BeginInvoke(DispatcherPriority.Normal,
                    (ThreadStart)delegate ()
                    {
                        CalculationResult.Text += "";
                    }
                );
            }

        }*/

        public static NodeList EvalParallel(NodeList inputlist)
        {
            NodeList currentList;
            int index;
            (NodeList changeList, int index) tuple;

            int i = 0;

            return null;
        }

        private static void Execute((NodeList changeList, int index) tuple, Dictionary<string, Combinator> combinators)
        {
            Obj ExecComb = tuple.changeList[tuple.index];
            Combinator dicComb = combinators[ExecComb.Meaning];
            NodeList currentlist = tuple.changeList;
            currentlist.RemoveAt(tuple.index);
            NodeList operands = new NodeList();
            int i = tuple.index;
            int j;

            for (j = 0; j < dicComb.Arity; j++)
            {
                operands.Add(currentlist[i]);

                currentlist.RemoveAt(i);
            }

            NodeList newList = Function(operands, combinators[ExecComb.Meaning].Result);

            for (j = newList.Count - 1; j >= 0; j--)
            {
                currentlist.Insert(i, newList[j]);
            }         
        }

        private static NodeList Function(NodeList operands, NodeList order)
        {
            Debug.WriteLine(operands.Count + " ffff");
            NodeList result = new NodeList();
            foreach (Obj temp in order)
            {
                if (temp is Listl)
                {
                    result.Add(new Listl(Function(operands, temp.List)));
                    continue;
                }
                int number = int.Parse(temp.Meaning) - 1;
                result.Add(operands[number]);
            }

            return result;
        }

        public static void UpList(NodeList list)
        {
            while (list[0] is Listl)
            {
                NodeList link = list[0].List;
                list.RemoveAt(0);
                for (int i = link.Count - 1; i >= 0; i--)
                {
                    list.Insert(0, link[i]);
                }
            }

            for (int i = 1; i < list.Count - 1; i++)
            {
                if (list[i] is Listl)
                {
                    if (list[i].List.Count == 1)
                    {
                        NodeList tempList = list[i].List;
                        list.RemoveAt(i);
                        list.Insert(i, tempList[0]);
                        continue;
                    }
                    Debug.WriteLine(i);
                    UpList(list[i].List);
                }
            }
        }


        public static void Optimize(NodeList list)
        {
            while (list[0] is Listl)
            {
                NodeList link = list[0].List;
                list.RemoveAt(0);
                for (int i = link.Count - 1; i >= 0; i--)
                {
                    list.Insert(0, link[i]);
                }
            }
            for (int i = 1; i < list.Count; i++)
            {
                if (list[i] is Listl)
                {
                    while (list[i] is Listl && list[i].List.Count == 1)
                    {
                        NodeList tempList = list[i].List;
                        list.RemoveAt(i);
                        list.Insert(i, tempList[0]);
                    }
                    Debug.WriteLine(i);
                    if (list[i] is Listl) Optimize(list[i].List);
                }
            }
        }


    }
}
