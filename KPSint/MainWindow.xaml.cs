﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using KPSint.Combinators;
using KPSint.List;
using KPSint.Service;
using Microsoft.Win32;

namespace KPSint
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int _i;
        private NodeList _currentList;
        private bool _stop;
        public string sumeval;
		ObservableCollection<CalculationResult> results;
        private string _resultText;
        private Dictionary<string, Combinator> _combinators;
        private bool _isSaved;
        private bool _interrupt;
		ObservableCollection<string> teslist;

        private delegate string Basis(string input);

        private Basis basis;

        public MainWindow()
        {
            InitializeComponent();
            _i = 0;
            _combinators = JsonParser.Deserialize("C:/Users/vlad/source/repos/KPSint/KPSint/Data/Combinators.json");
            Debug.WriteLine(_combinators.Count);
            _currentList = new NodeList();
            _stop = false;
            sumeval = "";
            _isSaved = false;
            _interrupt = false;
            results = new ObservableCollection<CalculationResult>();
			ResultList.ItemsSource = results;
        }

        private void Eval_click(object sender, RoutedEventArgs e)
        {
            EvalOneTime();
        }

        private void Enter_press(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter) EvalOneTime();
        }

        private void EndlessEval_click(object sender, RoutedEventArgs e)
        {
            EvalEndless();
        }

        private void Exit_click(object sender, RoutedEventArgs e)
        {
			Close();
        }

        private void Clean_click(object sender, RoutedEventArgs e)
        {
            if (!_isSaved)
            {
                string messageBoxText = "Данное действие приведет к удалению всей проделанной вами работы. " +
                                        "Вы уверены, что хотите очистить поле вывода?";
                string caption = "Очистка вывода";
                MessageBoxButton button = MessageBoxButton.YesNo;
                MessageBoxImage icon = MessageBoxImage.Warning;
                MessageBoxResult result = MessageBox.Show(messageBoxText, caption, button, icon);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        Release();
                        _i = 0;
                        _stop = false;
                        _resultText = "";
						results.Clear();
						_currentList = new NodeList();
                        _isSaved = true;
                        break;
                    case MessageBoxResult.No:
                        break;
                }

            }
            else
            {
                Release();
                _i = 0;
                _stop = false;
				_resultText = "";
				results.Clear();
				_currentList = new NodeList();
            }

        }

        private void Block()
        {
            InputStr.IsReadOnly = true;
            IKS.IsEnabled = false;
            IBCS.IsEnabled = false;
            IBWK.IsEnabled = false;
            CBWK.IsEnabled = false;
            IKSCBW.IsEnabled = false;
			Combs.IsEnabled = false;
        }

        private void Release()
        {
            InputStr.IsReadOnly = false;
            IKS.IsEnabled = true;
            IBCS.IsEnabled = true;
            IBWK.IsEnabled = true;
            CBWK.IsEnabled = true;
            IKSCBW.IsEnabled = true;
            Eval.IsEnabled = true;
			Combs.IsEnabled = true;
		}

        private void Stop_click(object sender, RoutedEventArgs e)
        {
            _interrupt = true;
            Release();
            Eval.IsEnabled = true;
            _stop = true;
            Stop.IsEnabled = false;
        }

		private void AddColumn()
		{

		}

        

        private void Save_click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.FileName = "Document";
            saveFile.DefaultExt = ".txt";
            saveFile.Filter = "Text documents (.txt)|*.txt";
            saveFile.OverwritePrompt = true;

            bool? result = saveFile.ShowDialog();

            if (result == true)
            {
                if (saveFile.FileName != "")
                {
                    string filename = saveFile.FileName;
                    System.IO.File.WriteAllText(filename, _resultText);
                    MessageBox.Show("Файл сохранен", "Успех!");
                    _isSaved = true;
                }
            }      

        }

        private void New_click(object sender, RoutedEventArgs e)
        {
            _currentList = new NodeList();
            Release();
            _resultText += "---------------------------------------------------------------------------------" + "\n";
            results.Add(new CalculationResult(string.Empty, "", ""));
            _i = 0;
            _stop = false;
        }

        private void IKS_OnChecked(object sender, RoutedEventArgs e)
        {
			IKS1.IsChecked = true;
			CBWK1.IsChecked = false;
			IBCS1.IsChecked = false;
			IBWK1.IsChecked = false;
			AllCombs.IsChecked = false;
			basis = Check.EditStrIKS;

        }

        private void IKSBWK_OnChecked(object sender, RoutedEventArgs e)
        {
				IKS1.IsChecked = false;
				CBWK1.IsChecked = false;
				IBCS1.IsChecked = false;
				IBWK1.IsChecked = false;
				AllCombs.IsChecked = true;
				basis = Check.EditStrIKSBWC;
	
        }

        private void CBWK_OnChecked(object sender, RoutedEventArgs e)
		{ 
				IKS1.IsChecked = false;
				CBWK1.IsChecked = true;
				IBCS1.IsChecked = false;
				IBWK1.IsChecked = false;
				AllCombs.IsChecked = false;
				basis = Check.EditStrCBWK;
        }

        private void IBCS_OnChecked(object sender, RoutedEventArgs e)
        {
				IKS1.IsChecked = false;
				CBWK1.IsChecked = false;
				IBCS1.IsChecked = true;
				IBWK1.IsChecked = false;
				AllCombs.IsChecked = false;
				basis = Check.EditStrIBCS;		
        }

        private void IBWK_OnChecked(object sender, RoutedEventArgs e)
        {
			IKS1.IsChecked = false;
			CBWK1.IsChecked = false;
			IBCS1.IsChecked = false;
			IBWK1.IsChecked = true;
			AllCombs.IsChecked = false;
			basis = Check.EditStrIBWK;
        }

        private void InputStr_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                EvalOneTime();
            }
        }

        private void EvalOneTime()
        {
            Block();

            string inputString = InputStr.Text;
            string newinputString;

            StringBuilder oldString;

            if (_currentList.Count == 0)
            {
                inputString = Check.EditStr(inputString);

                try
                {
                    Check.StringPreparing(inputString);
                    _resultText += "Исходное выражение: " + inputString + "\n";
                    newinputString = inputString;
                    inputString = basis(inputString);
                }
                catch (StringError strError)
                {
                    Release();
                    MessageBox.Show(strError.Message, "Ошибка!");
                    return;
                }

                _resultText += "0: " + inputString + "\n";
                //Result.Text += "0: " + inputString + "\n";
                results.Add(new CalculationResult("0", newinputString, inputString));

                oldString = new StringBuilder(inputString);

                _currentList = Parser.StringToList(oldString);

                New.IsEnabled = true;

            }
            else
            {
                oldString = Parser.ListToString(_currentList);
            }

            _currentList = Evaluation.Eval(_currentList, _combinators);

            StringBuilder newString = Parser.ListToString(_currentList);

            if (oldString.Equals(newString))
            {
                Release();
                _resultText += "Вычисление невозможно - получен окончательный результат\n";
                //Result.Text += "Вычисление невозможно - получен окончательный результат\n";
                _stop = true;
            }
            else
            {
                _resultText += (_i + 1) + ": " + newString + "\n";
                //Result.Text += (_i + 1) + ": " + newString + "\n";
                results.Add(new CalculationResult(Convert.ToString(_i+1), oldString.ToString(), newString.ToString()));
                _i++;
                _isSaved = false;
            }
        }

        private void EvalEndless()
        {
            /*Block();

            Stop.IsEnabled = true;

            Eval.IsEnabled = false;

            _interrupt = false;

            string inputString = InputStr.Text;
            string newInputString;

            StringBuilder oldString;

            if (_currentList.Count == 0)
            {

                inputString = Check.EditStr(inputString);

                try
                {
                    Check.StringPreparing(inputString);
                    _resultText += "Исходное выражение: " + inputString + "\n";
                    newInputString = inputString;
                    inputString = basis(inputString);
                }
                catch (StringError strError)
                {
                    Release();
                    MessageBox.Show(strError.Message, "Ошибка!");
                    return;
                }

                _resultText += "0: " + inputString + "\n";
                results.Add(new CalculationResult("0", new StringBuilder(newInputString), new StringBuilder(inputString)));

                oldString = new StringBuilder(inputString);

                _currentList = Parser.StringToList(oldString);

                New.IsEnabled = true;

            }
            else
            {
                oldString = Parser.ListToString(_currentList);
            }

            Stop.IsEnabled = true;

            if (!_stop)
            {
                Container cont = new Container(_currentList, CalculationResult, _i, Dispatcher);
                Thread endlessThread = new Thread(new ParameterizedThreadStart(ThreadEndlessEval));
                endlessThread.IsBackground = true;
                endlessThread.Start(cont);
                _isSaved = false;
            }
            else
            {
                Release();
                CalculationResult.Text += "Вычисление невозможно - получен окончательный результат\n";
            }*/
        }

        private void EvalWith_click(object sender, RoutedEventArgs e)
        {
            /*string firststr;
            if (_currentList.Count == 0)
            {
                firststr = InputStr.Text;
            }
            else
            {
                firststr = Parser.ListToString(_currentList).ToString();
            }
            try
            {
                Check.StringPreparing(firststr);
            }
            catch (StringError strError)
            {
                MessageBox.Show(strError.Message, "Ошибка!");
                return;
            }
            EvaluateWith evalWith = new EvaluateWith(firststr, CalculationResult);
            evalWith.Owner = this;
            evalWith.ShowDialog();
            if (evalWith.DialogResult == true)
            {
                MessageBox.Show("qqqqqqqq");
                try
                {
                    sumeval = basis(sumeval);
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                    throw;
                }

                if (!_stop)
                {
                    CalculationResult.Text += "Вычисление объекта прервано" + "\n";
                }

                _i = 0;
                CalculationResult.Text += "Выполнение комбинатора с присоединённым объектом" + "\n";
                CalculationResult.Text += _i + ": " + sumeval + "\n";

                StringBuilder oldString = new StringBuilder(sumeval);
                _currentList = Parser.StringToList(oldString);
                _stop = false;
                New.IsEnabled = true;
                Stop.IsEnabled = true;

                Container cont = new Container(_currentList, CalculationResult, _i, Dispatcher);
                Thread endlessThread = new Thread(new ParameterizedThreadStart(ThreadEndlessEval));
                endlessThread.IsBackground = true;
                endlessThread.Start(cont);
                _isSaved = false;
                _stop = true;
                Stop.IsEnabled = false;
            }
            else
            {
                sumeval = "";
            }*/
        }

        private void Stub(NodeList Node)
        {

        }

        private void ThreadEndlessEval(object inputo)
        {
            string result = "";
            Container input = (Container)inputo;
            int i = input.ind;
            NodeList beginlist = input.list;
            TextBlock Result = input.res;
            Dispatcher disp = input.disp;

            StringBuilder oldstring = Parser.ListToString(beginlist);
            NodeList newlist = Evaluation.Eval(beginlist, _combinators);
            StringBuilder newstring = Parser.ListToString(newlist);

            while (!oldstring.Equals(newstring) && i < 100 || _interrupt)
            {
                result += (i + 1) + ": ";
                result += newstring + "\n";
                oldstring = newstring;
                newlist = Evaluation.Eval(newlist, _combinators);
                _currentList = newlist;
                Evaluation.Optimize(newlist);
                newstring = Parser.ListToString(newlist);

                disp.BeginInvoke(DispatcherPriority.Normal,
                    (ThreadStart)delegate ()
                    {
                        Result.Text += result;
                    }
                );
                Thread.Sleep(70);
                i++;
                result = "";
            }
            if (_interrupt)
            {
                _interrupt = false;
                Result.Text += "Вычисление объекта прервано\n";
            }
            
            
        }

    }




}
