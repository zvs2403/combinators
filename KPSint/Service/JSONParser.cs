﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using KPSint.Combinators;
using Newtonsoft.Json;

namespace KPSint.Service
{
    public static class JsonParser
    {
        public static Dictionary<string, Combinator> Deserialize(string inputFile)
        {
            string source = ReadFile(inputFile);
			if(source == null)
			{
				MessageBoxImage icon = MessageBoxImage.Warning;
				MessageBoxButton button = MessageBoxButton.OK;
				MessageBox.Show("Не удалось найти файл с информацией о комбинаторах. Для использования доступны только комбинаторы I, K, S, C, B, W.", "Ошибка", button, icon);
				return Alarm();
			}
            Dictionary<string, Combinator> result = JsonConvert.DeserializeObject<Dictionary<string, Combinator>>(source);
            return result;
        }

        public static void Serialize(Dictionary<string, Combinator> combinators, string outputFile)
        {
            string result = JsonConvert.SerializeObject(combinators);
            WriteFile(result, outputFile);
        }

        private static string ReadFile(string input)
        {
            string result = string.Empty;
           
            try
            {
                using (StreamReader sr = new StreamReader(input))
                {
                    result = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
				return null;
            }
            Debug.WriteLine(result);
            return result;
        }

        private static void WriteFile(string output, string path)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(path, false, Encoding.Default))
                {
                    sw.WriteLine(output);
                }
 
                using (StreamWriter sw = new StreamWriter(path, true, Encoding.Default))
                {
                    sw.WriteLine(output);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

		public static Dictionary<string, Combinator> Alarm()
		{
			Dictionary<string, Combinator> alarm = new Dictionary<string, Combinator>();
			alarm.Add("I", new Combinator(1, Parser.StringToList(new StringBuilder("1"))));
			alarm.Add("K", new Combinator(2, Parser.StringToList(new StringBuilder("1"))));
			alarm.Add("S", new Combinator(3, Parser.StringToList(new StringBuilder("13(23)"))));
			alarm.Add("B", new Combinator(3, Parser.StringToList(new StringBuilder("1(23)"))));
			alarm.Add("C", new Combinator(3, Parser.StringToList(new StringBuilder("132"))));
			alarm.Add("W", new Combinator(2, Parser.StringToList(new StringBuilder("122"))));
			return alarm;
		}
    }
}
