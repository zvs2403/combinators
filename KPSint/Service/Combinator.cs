﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KPSint.List;
using Newtonsoft.Json;

namespace KPSint.Service
{
	public class Combinator
	{
	    [JsonProperty("arity")]
	    public int Arity { get; set; }
	    [JsonProperty("result")]
        [JsonConverter(typeof(NodeListConverter))]
	    public NodeList Result { get; set; }
	    public Combinator(int arity, NodeList list)
	    {
	        Arity = arity;
	        Result = list;
	    }

	    public override string ToString()
	    {
	        string r = Arity + " ";
	        r += Result.ToString();
	        return r;
	    }
	
	}
}
