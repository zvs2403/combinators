﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPSint.Service
{
	class CalculationResult
	{
		public string Number { get; set; }
		public string Term { get; set; }
		public string Result { get; set; }

		public CalculationResult(string number, string term, string result)
		{
			Number = number;
			Term = term;
			Result = result;
		} 
	}
}
