﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KPSint.List;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace KPSint.Service
{
    class NodeListConverter : JsonConverter
    {

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            StringBuilder value = new StringBuilder(reader.Value.ToString());
            NodeList result = Parser.StringToList(value);
            return result;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(Parser.ListToString((NodeList) value).ToString());
        }
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Combinator);
        }
    }
}
