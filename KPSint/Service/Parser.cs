﻿using System;
using System.Text;
using KPSint.List;
using KPSint.Combinators;

namespace KPSint.Service
{
    public class Parser
    {
        public static NodeList StringToList(StringBuilder input)
        {
            NodeList result = new NodeList();
            NodeList subresult;

            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] == ')')
                {
					throw new Exception("Проблема со скобкой в позиции " + i);
                }
                if (input[i] == '(')
                {
                    i++;
                    subresult = StringToListWithBrakets(input, ref i);
                    if (subresult == null)
                    {
						throw new Exception("Проблема со скобкой в позиции " + i);
                    }
                    Listl currentList = new Listl(subresult);
                    result.Add(currentList);
                }
                else
                {
                    Obj currentSymbol = new Combinators.Single(input[i].ToString());
                    result.Add(currentSymbol);
                }
            }

            //Debug.WriteLine(ShowMeAllLists(result));
            return result;
        }

        private static NodeList StringToListWithBrakets(StringBuilder input, ref int i)
        {
            NodeList result = new NodeList();
            NodeList subresult;

            for (; i < input.Length; i++)
            {
                if (input[i] == ')')
                {
                    return result;
                }
                if (input[i] == '(')
                {
                    i++;
                    subresult = StringToListWithBrakets(input, ref i);
                    if (subresult == null)
                    {
                        return null;
                    }
                    Listl currentList = new Listl(subresult);
                    result.Add(currentList);
                    }
                else
                {
                    Obj currentComb;
                    currentComb = new Combinators.Single(input[i].ToString());
                    result.Add(currentComb);
                }
            }
            return null;
        }

        public static StringBuilder ListToString(NodeList input)
        {
            if (input == null)
            {
                return new StringBuilder("");
            }
            StringBuilder result = new StringBuilder();
            foreach (var read in input)
            {
                if (!(read is Listl))
                {
                    result = result.Append(read.Meaning);
                }
                else
                {
                    result = result.Append('(').Append(ListToString(read.List)).Append(')');
                }
            }

            return result;
        }

        public static string ShowMeList(NodeList input)
        {
            if (input == null || input.Count == 0)
            {
                return "Null List to show";
            }
            string result = string.Empty;
            string subresult = string.Empty;
            int i = 0;
            //MessageBox.Show("Reach 139");
            foreach (var read in input)
            {
                if (!(read is Listl))
                {
                    subresult = subresult + read.Meaning + "--";
                }
                else
                {
                    i++;
                    subresult = subresult + i;
                    result = ShowMeList(read.List, ref i) + result;
                    subresult = subresult + "--";
                    
                }
            }
            //MessageBox.Show("Reach 155");
            subresult = subresult.Remove(subresult.Length - 2, 2);
            result = result + subresult + "\n" + "------------------------------------------------" + "\n";

            return result;

        }

        private static string ShowMeList(NodeList input, ref int level)
        {
            StringBuilder result = new StringBuilder();
            StringBuilder subresult = new StringBuilder(level + ": ");
            foreach (var read in input)
            {
                if (!(read is Listl))
                {
                    result = result.Append(read.Meaning).Append("--");
                }
                else
                {
                    level++;
                    result.Append(level);
                    ShowMeList(read.List, ref level);
                    result = result.Append("--");
                }
            }
            result.Remove(result.Length - 2, 2);
            //Console.WriteLine(result);
            return "";
        }

        public static string ShowMeOneList(NodeList input)
        {
            string result = "List: ";
            int i = 0;
            foreach (var read in input)
            {
                if (!(read is Listl))
                {
                    result += read.Meaning + "--";
                }
                else
                {
                    i++;
                    result += i;

                    result += "--";
                }
            }

            string remove = result.Remove(result.Length - 2, 2);
            return remove;
        }

    }
}
