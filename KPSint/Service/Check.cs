﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using KPSint.Combinators;
using KPSint.List;
using System.Diagnostics;


namespace KPSint.Service
{
    class Check
    {
        public static void CheckString(string inputString)
        {

            if (inputString == string.Empty)
            {
                throw new StringError("На входе пустая строка");
            }

            int leftBrackets = 0;
            int rightBrackets = 0;
            foreach (var ch in inputString)
            {
                if (ch == '(')
                {
                    leftBrackets++;
                }

                if (ch == ')')
                {
                    rightBrackets++;
                }
            }

            if (leftBrackets != rightBrackets)
            {
                throw new StringError("Не совпадает количество открывающих и закрывающих скобок");
            }

        }

        public static void StringPreparing(string inputString)
        {
            CheckString(inputString);

            StringBuilder oldString = new StringBuilder(inputString);


            try
            {
                NodeList currentList = Parser.StringToList(oldString);
            }
            catch (Exception ex)
            {
                throw new StringError(ex.Message);
            }

        }

        public static string EditStr(string input)
        {
            string res;
            res = input.Replace(" ", string.Empty);
            res = input.Replace("()", string.Empty);
            return res;
        }

        public static string EditStrIKS(string input)
        {
            string res = input;

            res = res.Replace("W", "(SS(K(SKK)))");
            res = res.Replace("C", "(S((S(KS)K)(S(KS)K)S)(KK))");
            res = res.Replace("B", "(S(KS)K)");
            
            return res;
        }

        public static string EditStrIKSBWC(string input)
        {
            return input;
        }

        public static string EditStrIBCS(string input)
        {
            int kIndex = input.IndexOf("K");
            int wIndex = input.IndexOf("W");

            if (kIndex != -1)
            {
                throw new StringError("Невозможно вычислить данный объект в выбранном базисе! Причина: комбинатор К в позиции " + kIndex);
            }
            if (wIndex != -1)
            {
                throw new StringError("Невозможно вычислить данный объект в выбранном базисе! Причина: комбинатор W в позиции " + wIndex);
            }
            return input;
        }

        public static string EditStrIBWK(string input)
        {
            int sIndex = input.IndexOf("S");
            int cIndex = input.IndexOf("C");

            if (sIndex != -1)
            {
                throw new StringError("Невозможно вычислить данный объект в выбранном базисе! Причина: комбинатор S в позиции " + sIndex);
            }
            if (cIndex != -1)
            {
                throw new StringError("Невозможно вычислить данный объект в выбранном базисе! Причина: комбинатор C в позиции " + cIndex);
            }
            return input;
        }

        public static string EditStrCBWK(string input)
        {
            string res = input;

            res = res.Replace("I", "(WK)");
            res = res.Replace("S", "(B(BW)(BBC))");
            return res;
        }

        public static void UpList(NodeList list)
        {
            StringBuilder s = Parser.ListToString(list);
            while (list[0] is Listl)
            {
                NodeList link = list[0].List;
                list.RemoveAt(0);
                for (int i = link.Count - 1; i >= 0; i--)
                {
                    list.Insert(0, link[i]);
                }

            }
            s = Parser.ListToString(list);

            for (int i = 1; i < list.Count - 1; i++)
            {
                if (list[i] is Listl)
                {
                    if (list[i].List.Count == 1)
                    {
                        NodeList tempList = list[i].List;
                        list.RemoveAt(i);
                  }
                    Debug.WriteLine(i);
                    UpList(list[i].List);
                }
            }
        }
    }
}