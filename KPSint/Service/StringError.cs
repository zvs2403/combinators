﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPSint.Service
{
    class StringError : Exception
    {
        public StringError(string message)
            : base(message)
        { }
    }
}
