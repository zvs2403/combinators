﻿using KPSint.List;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Threading;

namespace KPSint.Service
{
	public class Container
	{
		public Container(NodeList list, TextBlock res, int ind, Dispatcher disp)
		{
			this.list = list;
			this.res = res;
			this.ind = ind;
		    this.disp = disp;
		}

		public NodeList list;
		public TextBlock res;
		public int ind;
	    public Dispatcher disp;
	}
}
