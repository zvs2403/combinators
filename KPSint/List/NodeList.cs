﻿using KPSint.Combinators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KPSint.Service;

namespace KPSint.List
{
    public class NodeList : List<Obj>, ICloneable
    {
        //public NodeList prevList;
        //public int prevIndex;
        //public int level;

        /*public NodeList(NodeList prevL, int prevI, int level)
        {
            prevList = prevL;
            prevIndex = prevI;
            this.level = level;
        }


        public NodeList()
        {
            prevList = null;
            prevIndex = -1;
            level = 0;
        }

        public NodeList(int level)
        {
            prevList = null;
            prevIndex = -1;
            this.level = level;
        }*/

        public NodeList()
        {

        }

        public object Clone()
        {
            NodeList clonedList = new NodeList();
            Obj cloneNode; 
            foreach (Obj node in this)
            {
                cloneNode = node.Clone();
                clonedList.Add(cloneNode);
            }
            return clonedList;
        }

        public override string ToString()
        {
            return Parser.ListToString(this).ToString() + " ";
        }
    }
}
