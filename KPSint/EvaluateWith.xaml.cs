﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using KPSint.List;
using KPSint.Service;


namespace KPSint
{
    /// <summary>
    /// Логика взаимодействия для EvaluateWith.xaml
    /// </summary>
    public partial class EvaluateWith : Window
    {
        private string _firststring;
        private TextBlock result;

        public EvaluateWith()
        {
            InitializeComponent();
        }

        public EvaluateWith(string firsts, TextBlock Result)
        {
            InitializeComponent();
            _firststring = firsts;
            result = Result;
        }

        public void Close_click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();

        }

        public void Evaluate_click(object sender, RoutedEventArgs e)
        {
            string firststr = _firststring;
            string secondstr = InputStr.Text;

            firststr = Check.EditStr(firststr);

            try
            {
                Check.StringPreparing(secondstr);
            }
            catch (StringError strError)
            {
                MessageBox.Show(strError.Message, "Ошибка!");
                return;
            }

            DialogResult = true;

            Close();

            string resstr = firststr + secondstr;

            MainWindow main = this.Owner as MainWindow;

            main.sumeval = resstr;

        }
    }
}
